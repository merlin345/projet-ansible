## Projet 

Par groupe de 2 à 5 vous allez mettre en place des script Ansible pour provisionner une stack Wordpress.

Vous allez donc devoir créer des roles pour provisionner 2 VM, une contenant la Wordpress lui même et l'autre avec la base de donnée. 

Le rendu sera sous la forme d'un projet Gitlab contenant vos scripts Ansible. 

Pour le role d'installation de wordpress vous allez devoir : 
- Récupérer les binaire de Wordpress
- Installer la version de PHP correspondante
- Copier le ficher de configuration
- etc...

Pour le role d'installation de mysql : 
- Installer MySQL/MariaDB
- Créer un base de donnée
- Créer un user/setup son password
- Autoriser le user à se connecter en remote
- etc...