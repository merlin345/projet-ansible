# Prérequis

Pour ce TP nous allons avoir besoin de 3 VM Linux (Debian de préférence) : 

- 1 VM "Master", avec 2 GO de RAM et 2 CPU (CLI, pas d'interface)
- 2 VM "Slave", avec __minimum__ 512Mo de RAM et 1 CPU (CLI, pas d'interface)

Ansible fonctionne grâce au SSH,la première étape va donc être d'installer un serveur SSH sur toutes les machines avec la commande suivante : 

```bash
apt update
apt install openssh-server -y
```

> :warning:
>
> N'oubliez pas d'autoriser la connexion en root dans la config du SSH (/etc/ssh/sshd_config)
>

Pour que le Master puisse envoyer ses intructions aux slaves vous devez, dans un premier temps, copier la clé SSH depuis votre machine master avec la commande (pour des questions de simplicité nous utiliserons l'utilisateur root, pensez à autoriser la connexion en SSH avec le user root) : 

```bash
# SUR LE MASTER
# Créer un clé SSH avec le user root
HOSTNAME=`hostname` ssh-keygen -t rsa -C "$HOSTNAME" -f "$HOME/.ssh/id_rsa" -P "" && cat ~/.ssh/id_rsa.pub
# Puis copier la clé SSH généré sur les machines
ssh-copy-id -i </PATH/TO/PUBLIC/KEY> root@<IP-SLAVE1>
ssh-copy-id -i </PATH/TO/PUBLIC/KEY> root@<IP-SLAVE2>
# Puis le password de root 
```

> :warning:
> 
> ## Vérification 

Une fois ces deux étapes effectuées, connectez vous en SSH depuis le master vers slave avec : 

```bash
 ssh -i </PATH/TO/PRIVATE/KEY> root@<IP>
```

Si cela vous demande un mot de passe c'est que votre clé n'a pas été copié, repartez à l'étape 1.

Puis vous allez installer ansible sur le master avec la commande : 

```bash
apt update
apt install ansible
```

## L'inventaire 

Sur le master

Pour commencer nous allons créer un inventaire, pour cela nous allons créer un dossier nommé _inventory_, avec à l'intérieur un fichier nommé _hosts_ : 

```bash
mkdir inventory
touch inventory/hosts
```

et le contenu du fichier _hosts_ sera comme ceci (avec les ip de VOS machines): 

```
[web]
172.16.1.2
[db]
172.16.1.3
```

L'inventaire est plus complexe qu'il n'y parait au premier abord, vous pouvez à chaque host lui donner des variables propre à lui meme, comme par exemple le user que ansible devra utiliser (si vous n''utilisez pas root). Voici le lien vers la documentation pour approfondir cela [Documentation](https://docs.ansible.com/ansible/latest/user_guide/intro_inventory.html).

### AD HOC

Le Ansible AD HOC est un one-liner qui permet d'exécuter une instruction Ansible sur un inventaire donné. Pour le tester nous allons faire la commande suivante : 

```bash
ansible -i inventory/hosts -m ping web
```

- Option -i pour préciser l'inventaire
- Option -m pour préciser le module
- Option -a (non utilisé ici) pour mes options du module
- Le dernier paramètre est le serveur ou le groupe de serveurs dans l'inventaire 

Quelle commande faudrait il effectuer pour lancer l'instruction sur l'ensemble des machines de notre inventaire ? 

Nous allons maintenant utiliser le module _copy_ 

```bash
ansible -i inventory/hosts -m copy -a 'dest=/tmp/tp_epsi content="hello world"' web
```

### Idempotence

Maintenant nous allons parler d'une notion importante de Ansible qui est l'idempotence. Une opération est dites idempotente SI «  si elle donne exactement le même résultat que vous l'exécutiez une fois ou de manière répétée, sans aucune intervention ». cf Red Hat 

La conséquence est que vous pouvez exécuter vos playbooks plusieurs fois et que le résultat final devrait rester le même : les serveurs ciblés seront dans leur « état souhaité ». Si votre playbook échoue sur un petit nombre de serveurs, vous pouvez résoudre le problème et l'exécuter à nouveau. Puisque le playbook est idempotent, les serveurs ciblés seront dans leur « état souhaité » et aucun autre changement ne se produira.

Vous allez refaire la commande d'avant, quelle différence voyez vous ?

Et maintenant, refaites la commande en changeant le contenu du fichier : 

```bash
ansible -i inventory/hosts -m copy -a 'dest=/tmp/tp_epsi content="hello world !!!!! "' web
```

Vous devez observer un changement, lequel ? 

## Exercice 1 

Vous allez maintenant réaliser les étapes suivantes, en utilisant le AD HOC (Lien vers la [documentation des modules](https://docs.ansible.com/ansible/2.8/modules/list_of_all_modules.html)): 

- Afficher la place disponible avec le module command
- Installer le paquet tcpdump avec le module apt
- Ajouter un utilisateur avec le module user
- Copier votre fichier local /etc/passwd dans /tmp (de chaque serveur) avec le module copy
- Récupérer le fichier /etc/passwd (de chaque serveur) avec le module fetch
- Créer le dossier /tmp/epsi avec le module file

## Les playbook

Un playbook est le fichier YAML qui va décrire la configuration de vos serveurs, comme sont nom l'indique, un _playbook_ est une liste de _play_ et un play est liste de _tasks_ à faire sur des hosts donnés. Une _task_ est une tâche unique à faire, en utilisant un module, et pour finir, une _module_ fournit des fonctionnalités précises (file, user, mysql...).

Voici un exemple de playbook : 

```yml
---
- name: Installs and configure web servers
  hosts: web
  tasks:
    - name: Install Apache
      apt: 
        name: apache2
        state: present
        update_cache: yes
    - name: Start Apache
      service: 
        name: apache2
        state: started 
        enabled: yes

- name: Installs and configure db servers
  hosts: db
  tasks:
    - name: Install MySQL
      apt: 
        name: mariadb-server 
        state: present
    - name: Start MySQL
      service: 
        name: mysql 
        state: started 
        enabled: yes
```

Et la commande pour le lancer : 

```bash
ansible-playbook -i inventory/hosts playbook.yml
```


## Notes sur les playbooks

- Le playbook est joué de haut en bas
- Le playbook s'arrête en cas d'erreur
- Si l'on relance le playbook (non modifié) : il faut qu'il n'y ait aucun changement (C'est l'idempotence que nous avons vu plus haut)
	- Le run sera beaucoup plus rapide
- Conseil : toujours mettre un nom aux "plays" et aux "tasks"

## Utilisateur et sudo

```
---
- hosts: web
  remote_user: user
  become: yes
```

- L'utilisateur _user_ doit avoir des droits "sudo" configurés
- Si un password est nécessaire : --ask-become-pass
- Note : "become: yes" peut être utilisé au niveau d'une unique "task"

Comme nous avons configuré les machines pour une connexion en root, le _become_ n'est pas forcément nécessaire 


## Exercice 2

Vous allez maintenant créer un playbook contenant les plays ci dessous pour le serveur de la catégorie \[web\] : 

- Installation apache2 + php7.4 ou (si debian 12 php8.2) 
- Apache2 lancé et activé
- Suppression fichier /var/www/html/index.html
- Création fichier /var/www/html/index.php (contenant un phpinfo)
- Droits : root:www-data rw-r-----

## Exercice 3 

Votre playbook est maintenant opérationnel, vous allez tester plusieurs choses : 

- Tester l'idempotence
- Se connecter en SSH au serveur
- Modifier le fichier /var/www/html/index.php
- Arrêter apache et se déconnecter
- Relancer le playbook

# Variables

## Emplacement

- On peut les renseigner à plusieurs endroits
	- Ligne de commande
	- Inventaire
	- Rôle
	- Playbook
	- Facts
	- Tâches

## Variables dans un playbook

```
- hosts: web
  vars:
    http_port: 80
```

## Variables dans un fichier séparé

Vous allez modifier le fichier  _playbook.yml_ et créer le fichier _vars.yml_, avec leurs contenu respectif décris en dessous.

playbook.yml : 

```yml
---
- name: Include variables
  hosts: all
  tasks:
    - include_vars:
        file: ./vars.yml
[...]
```

vars.yml : 

```yml
apache_package_name: apache2
mysql_package_name: mariadb-server
```

## Dossier group_vars

Les fichier de variables dans le dossier _group\_vars_ s'applique aux groupes de votre inventaire

group_vars/all.yml : 

```yml
acl_admin_ip: 172.16.1.1
```

group_vars/web.yml : 

```yml 
apache_package_name: apache2
```

group_vars/db.yml

```yml
mysql_package_name: mariadb-server
```

## Dossier host_vars

Les fichier de variables dans le dossier _host\_vars_ s'applique aux hosts précis de votre inventaire.

host_vars/172.16.1.3.yml :

```yml
mysql_package_name: mysql-server
```

> :warning:
>
> Mais la méthode la plus facile pour gérer les variables spécifiques à un host en particulier est de les déclarer dans le fichier inventory sur la ligne de déclaration de l'host
>

## Types de variables

```
apache_version: 2.4
php_modules:
  - php-gd
  - php-mysql
  - php-mail
ssh_keys_users:
  user: ssh-rsa AAAAAA
  db-user: ssh-rsa BBBBB
users:
  user:
    uid: 1000
    gid: 1000
    shell: /bin/bash
    home_dir: /home/user
  db-user:
    uid: 1001
    gid: 1001
    shell: /usr/bin/zsh
    home_dir: /var/www/db-user
```


## Utilisation des variables dans un playbook

- Syntaxe Jinja2
- Filtres Jinja2

```
- name: Installs and configure web servers
  hosts: web
  tasks:
    - name: Install Apache
      apt: name="{{ apache_package_name }}" state=present
    - name: Start Apache
      service: name=apache2 state=started enabled=yes

- name: Installs and configure db servers
  hosts: db
  tasks:
    - name: Install MySQL
      apt: name="{{ mysql_package_name }}" state=present
    - name: Start MySQL
      service: name=mysql state=started enabled=yes
```

## Boucles sur une liste

- Deux méthodes : ```loop``` et ```with_```

```
- hosts: web
  tasks:
    - name: Install Apache
      apt: 
        name: {{ item }} 
        state: present
      with_items: "{{ php_modules }}"
```

```
- hosts: web
  tasks:
    - name: Install Apache
      apt: 
        name: "{{ item }}" 
        state: present
      loop: "{{ php_modules }}"
```

## Boucles sur un dictionnaire

```
- name: Boucle sur un dictionnaire
  hosts: all
  tasks:
     - name: Boucle sur un dictionnaire simple
       debug: 
        msg="SSH key of {{ item.key }} is {{ item.value }}"
      loop: "{{ lookup('dict', ssh_keys_users) }}"
     - name: Boucle sur un dictionnaire imbriqué
       debug:
         msg="L'utilisateur {{ item.key }} a pour UID {{ item.value.uid }} et shell {{ item.value.shell }}"
       loop: "{{ lookup('dict', users) }}"
```

## Facts

- Les facts sont des variables accessibles par défaut
- Elles décrivent des propriétés du serveur
	- Distribution, adresses ip...
- On peut les visualiser grâce au module setup

```
ansible -i inventory -m setup all 
[...]
        "ansible_distribution": "Debian",
        "ansible_distribution_major_version": "10",
        "ansible_distribution_release": "buster",
        "ansible_distribution_version": "10.1",
```

## Conditions

Faites un nouveau playbook _playbook2.yml_ avec le contenu suivant :

```yml
---
- name: Installs and configure web servers
  hosts: web
  tasks:
    - name: Install Apache on Debian
      apt: 
        name: "{{ apache_package_name }}" 
        state: present
      when: ansible_distribution == "Debian"
    - name: Install Apache on Centos
      apt: 
        name: "{{ apache_package_name }}" 
        state: present
      when: ansible_distribution == "RedHat"
    - name: Start Apache
      service: 
        name: apache2 
        state: started 
        enabled: yes
```

## Inclusion de variables en fonction de la distribution


```yml
---
- name: Set specific variables for distributions
  include_vars: '{{ item }}'
  with_first_found:
    - files:
        - '{{ ansible_distribution }}-{{ ansible_distribution_major_version }}.yml'
        - 'main.yml'
        - paths: '../vars'
```

## Templates

- Les variables sont accessibles dans les templates (module template)
- Les templates sont écrits en syntaxe jinja2

## Roles et playbook avancé

Nous avons vu comment faire des playbook basiques, maintenant nous allons mettre en place des playbook plus avancés en utilisant des _roles_, pour cela nous allons devoir setup une architecture spécifique de nos dossiers. Nous allons nous baser sur l'organisation type "ansible-galaxy" détaillé dans la partie [_Best pratice_](https://docs.ansible.com/ansible/2.8/user_guide/playbooks_best_practices.html#) de Ansible.

Voici un exemple d'a quoi doit ressembler notre dossier : 

```lua
production                # inventory file for production servers
staging                   # inventory file for staging environment

group_vars/
   group1.yml             # here we assign variables to particular groups
   group2.yml
host_vars/
   hostname1.yml          # here we assign variables to particular systems
   hostname2.yml

site.yml                  # master playbook
webservers.yml            # playbook for webserver tier
dbservers.yml             # playbook for dbserver tier

roles/
    common/               # this hierarchy represents a "role"
        tasks/            #
            main.yml      #  <-- tasks file can include smaller files if warranted
        handlers/         #
            main.yml      #  <-- handlers file
        templates/        #  <-- files for use with the template resource
            ntp.conf.j2   #  <------- templates end in .j2
        files/            #
            bar.txt       #  <-- files for use with the copy resource
            foo.sh        #  <-- script files for use with the script resource
        vars/             #
            main.yml      #  <-- variables associated with this role
        defaults/         #
            main.yml      #  <-- default lower priority variables for this role
        meta/             #
            main.yml      #  <-- role dependencies
        library/          # roles can also include custom modules
        module_utils/     # roles can also include custom module_utils
        lookup_plugins/   # or other types of plugins, like lookup in this case

    webtier/              # same kind of structure as "common" was above, done for the webtier role
    monitoring/           # ""
    fooapp/               # ""
```

Le dossier _roles_ va contenir les roles que vous allez créer. 

## Setup de l'environnement 

Vous allez commencer par créer un dossier du nom de votre choix (ici j'utiliserai le nom _tp_). Et dans ce dossier je créerai 2 sous dossier _roles_ and _group\_vars_:

```bash
mkdir -p tp/{roles,group_vars}
```

L'objectif va être de créer des petits "modules" qui effectuent des actions simple et qu'ils puissent être réutilisables pour d'autre actions. Nous allons dans notre cas créer 3 roles : 

- base, pour une première configuration de serveur qui sera utile peut importe ce qui sera installé dessus.
- nginx, pour l'installation et la config d'un serveur nginx de base.
- mysql, pour l'installation et la configuration de base d'un serveur MySQL/MariaDB de base.

Pour cela nous allons nous mettre à la racine de "roles", et faire les commandes suivantes : 

```bash
ansible-galaxy init --offline base
ansible-galaxy init --offline nginx
ansible-galaxy init --offline mysql
#Puis aller créer le dossier templates dans chacun des répertoires 
mkdir -p {base,nginx,mysql}/templates
```

## Configuration du role base 

Dans un premier temps nous allons nous concentrer sur le role "base" qui va être le role qui se joue sur toutes nos machines, c'est le role de base (d'où le nom...) pour avoir la même configuration système sur notre parc.

Dans le fichier ```base/tasks/main.yml``` vous allez ajouter la configuration suivante : 

```yaml
---
- name: Change hostname
  hostname:
    name: "{{ inventory_hostname }}"

- name: Update source list
  apt:
    update_cache: yes

- name: Update the system
  apt:
    name: "*"
    state: latest

- name: Install minimal packages
  apt:
    name: ['curl', 'vim', 'wget','jq'] 
    state: present

- name: Copy motd
  template:
    src: motd.j2
    dest: /etc/motd
```

Connaissez vous motd dans linux ainsi son utilité ? 

Vous aller maintenant créer le fichier ```base/templates/motd.j2``` avec le contenu suivant : 

```j2
Welcome to "{{ inventory_hostname }}".
```

Les fichiers .j2 sont des fichiers de template, leur avantage est de pouvoir interpréter des variables à la volée, ce qui permet de déployer des fichiers sur les serveurs cibles sans avoir à faire la moindre modification dessus.


## Configuration du role nginx

Nous allons maintenant nous occuper de la configuration du role "nginx".

Pour cela nous allons éditer le fichier ```nginx/tasks/main.yml``` en mettant la configuration suivante : 

```yaml
---
- name: Install nginx
  apt:
    name: 'nginx'
    state: 'latest'
  notify: start nginx

#Attention, selon votre distibution linux, la destination ne sera pas la même (ici debian 11)
- name: Create index.html
  template:
    src: index.html.j2
    dest: /usr/share/nginx/html/index.html

- name: Change worker_connections
  template:
    src: nginx.conf.j2
    dest: /etc/nginx/nginx.conf
  notify: restart nginx
```

Vous allez maintenant aller modifier le fichier ```nginx/handlers/main.yml``` avec le contenu suivant : 

```yml
---
- name: start nginx
  systemd:
    name: 'nginx'
    state: 'started'
    enabled: yes

- name: restart nginx
  systemd:
    name: 'nginx'
    state: 'restarted'
```

Petite parenthèse sur les handlers, les handlers sont des actions qui vont se déclencher "on change" d'une task. Si la task change l'état du server, le paramètre "notify" va aller chercher l'action a effectuer dans le fichier "handlers/main.yml", et le nom du "notify" doit matcher avec le "name" du handler. Il effectuera ensuite l'action spécifié. En général il est utilisé pour redémarrer un service après un changement de configuration.

## Exercice 4

Vous allez maintenant devoir créer 2 fichier de template, celui pour "index.html.j2", qui devra contenir l'IP de la machine, et "nginx.conf.j2" qui contiendra la configuration nginx avec le paramètre ```worker_connections``` à 8192

## Configuration du role mysql

Pour finir nous allons configurer le role MySQL, pour cela nous allons éditer le fichier ```mysql/tasks/main.yml``` : 

```yml
- name: Install mysql
  apt:
    name: 'mariadb-server'
    state: 'latest'
  notify: start mysql
```

Et ensuite la configuratio du handler ```mysql/handlers/main.yml```: 

```yml
---
- name: start mysql
  systemd:
    name: 'mysql'
    state: 'started'
    enabled: yes
```

## Main et inventory

Maintenant que tous les roles sont configurer nous allons retourner à la racine du dossier "tp" et créer le fichier main.yml contenant le code suivant : 

```yml
---
- hosts: all
  roles:
  - base

- hosts: web
  roles:
    - nginx

- hosts: db
  roles:
    - mysql
```

Et pour finir le fichier d'inventaire, vous allez d'abord créer un dossier nommé "inventory", et vous allez créer le fichier "hosts" à l'intérieur avec la configuration suivante : 

```
[web]
nginxserver1 ansible_host=<IP_HOST1>
[db]
mysqlserver1 ansible_host=<IP_HOST2>
```

Maintenant vous allez exécuter la commande suivante depuis la racine du dossier "tp" :

```bash
ansible-playbook -i inventory/hosts main.yml
```

Félicitations vous savez maintenant comment fonctionne les roles.

## Exercice 5 

Vous allez dans un premier temps créer une VM supplémentaire pour faire cette exercice.

Vous allez créer un role supplémentaire nommé "gitlab-runner", qui aura pour but de d'installer docker, récupérer une image sur le docker hub, l'image gitlab/gitlab-runner:latest, et ensuite de lancer un container comme si vous le lanciez avec la commande suivante : 

```bash
docker run -d --name gitlab-runner --restart always -v /root/runner-config:/etc/gitlab-runner -v /var/run/docker.sock:/var/run/docker.sock gitlab/gitlab-runner:latest
```

Ce qui veut dire que les dossier doivent exister, etc... 

Il faudra de plus rajouter une section \[runner\] dans le fichier hosts faisant référence à la nouvelle VM.

Vous rajouterez ensuite une section dans le fichier main.yml pour que le role s'exécute uniquement sur le groupe \[runner\].
 